********************************************
DOC - Git : Spécifications générales des entrepôts
********************************************

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
  JLE          30/12/2022       V1.0
========= ==================== =======

Dépôts
=====================

**Compte GitHub** (Dépôts publiques) : https://github.com/CEO-Vision

.. note::
   Ce compte n'est plus activement utilisé et les dépôts sont progressivement migrés vers GitLab

**Groupe de dépôts Publiques** : https://gitlab.com/ceovision

**Groupe de dépôts Privés** : https://gitlab.com/ceo-vision

GoFAST V3
-----------

**Localisation** : https://gitlab.com/ceo-vision/gofast3

Ce dépôt est le dépôt principal des développeurs, il contient le code source de Drupal avec tous nos modules.

Se référer aux documentations de la section "Documentation développeur" pour l'usage.

GoFAST Install
-----------------

**Localisation** : https://gitlab.com/ceo-vision/gofast-install

Ce dépôt contient tous les scripts, toutes les sources et les configurations servant à déployer ou mettre à jour GoFAST

.. WARNING::
   Ce dépôt deviendra obsolète en 2023/2024 une fois que les plateformes GoFAST auront migrés vers l'architecture NG
   De ce fait, peu de documentation sera fournie à ce sujet

**TODO** : Faire une documentation minimale au besoin

GoFAST NG APP
-----------------

"APP" étant de le nom de l'application concernée (Alfresco, Drupal, ...).

Il s'agit de dépôts de containers GoFAST en architecture NG.

.. WARNING::
   Ces dépôts font partie de la nouvelle architecture de GoFAST et possèdent des conventions
   plus strictes que les dépôts historiques

Se référer a la documentation "Architecture NG" de la section "Documentation CI/CD".

**TODO** : Ecrire cette documentation depuis https://gofast.ceo-vision.com/node/171938
