********************************************
PROC - Accès et outils
********************************************

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
  JLE          30/12/2022       V1.0
========= ==================== =======

Outils
=====================

Outils obligatoires
---------------------

Les outils ou applications suivants sont obligatoires sur chaque postes développeur

* Poste de travail

  * Mise à jour régulière du poste

  * Nommage du poste : **CEOV-GROUP-TYPE-ID** => Demander un identifiant à votre N+1

  * Nommage des comptes

    * Un compte Administrateur nommé "admin" pour Windows et "root" pour Linux et MacOS

    * Un compte utilisateur non Administrateur avec votre nom d'utilisateur entreprise (1ère lettre du prénom + nom)

  * Installation de l'antivirus Bitdefender

    * Windows : https://cloudgz.gravityzone.bitdefender.com/Packages/BSTWIN/0/setupdownloader_[aHR0cHM6Ly9jbG91ZGd6LWVjcy5ncmF2aXR5em9uZS5iaXRkZWZlbmRlci5jb20vUGFja2FnZXMvQlNUV0lOLzAvczhxUlNQL2luc3RhbGxlci54bWw-bGFuZz1mci1GUg==].exe

    * Mac : https://cloudgz.gravityzone.bitdefender.com/Packages/MAC/0/s8qRSP/setup_downloader.dmg

    * Linux : https://cloudgz.gravityzone.bitdefender.com/Packages/NIX/0/s8qRSP/setup_downloader.tar

  * Installation de la politique de sécurité

    * Windows

      * Télécharger https://gofast.ceo-vision.com/node/151138

      * Dézipper dans "C:\Windows\System32\GroupPolicy"

      * Télécharger https://gofast.ceo-vision.com/node/150847 et placer dans "C:\Users\USER_ID\Documents\Security\Templates"

      * Executez la commande "gpupdate /force"

* GitKraken => **TODO** : Documenter l'intégration de la licence

Outils conseillés
---------------------

* IDE : Visual Studio Code, PHPStorm, Netbeans

Accès
=====================

En attente de l'interface de gestion de comptes (account), demandez vos accès à votre N+1 : 

- Annuaire interne

- GoFAST

- Messagerie Bluemind

- Jira

- Teampass

- Gitlab

**TODO** : A documenter une fois l'interface https://account.ceo-vision.com mise en production.