**********************************************
Procédure : Mise à jour des certificats GOFAST
**********************************************

Cette procédure décrit comment effectuer la mise à jour des certificats GOFAST.

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
BCR        12/09/2022           V1.0
========= ==================== =======

Pré-requis :
============

- Les certificats (wildcard ou non) au format ...
- Un accès aux machines **MAIN** & **COMM**

Installation :
==============

Machine MAIN
------------

::
    
    # Définition des variables nécessaires
    echo "Entrer votre URL GoFAST MAIN :" && read gofast_main
    echo "Entrer votre URL GoFAST MOBILE :" && read gofast_mobile

    # Pour commencer, on stop les services web.
    systemctl stop httpd
    systemctl stop nginx

    # Puis, on fait un backup des certificats existants :
    mkdir /opt/backup_certs
    cp /etc/pki/tls/certs/localhost.crt /opt/backup_certs/localhost.crt
    cp /etc/pki/tls/private/localhost.key /opt/backup_certs/localhost.key

    # Ensuite, il faut lancer le scrip acme.sh (situé dans /opt) :
    ./acme.sh --renew -d $gofast_main --insecure

    # Enfin, on copie les certificats dans les répertoires pour les remplacer :
    cp /root/.acme.sh/$gofast_main/$goafst_main.cer  /etc/pki/tls/certs/localhost.crt
    cp /root/.acme.sh/$goafst_main/$goafst_main.key /etc/pki/tls/private/localhost.key

    # Toujours sur la machine **MAIN**, il faut mettre à jour les certificats mobile (si le certificat n'est pas wildcard).
    # Pour ce faire, il faut de nouveau lancer le scrip acme.sh.

    cp /etc/pki/tls/certs/localhost_mobile.crt  /opt/backup_certs/localhost_mobile.crt
    cp /etc/pki/tls/private/localhost_mobile.key /opt/backup_certs/localhost_mobile.key
    ./acme.sh --renew -d $gofast_mobile --insecure
    cp /root/.acme.sh/$gofast_mobile/$gofast_mobile.cer /etc/pki/tls/certs/localhost_mobile.crt
    cp /root/.acme.sh/$gofast_mobile/$gofast_mobile.key /etc/pki/tls/private/localhost_mobile.key

    # Pour finir, on redémarre les services web de la machine.
    systemctl start httpd
    systemctl start nginx

Machine COMM
------------

::

    # Définition de la variable nécessaire
    echo "Entrer votre URL GoFAST COMM :" && read gofast_comm
    
    # On commence par stopper les services nginx et coturn, et on backup les anciens certificats :
    systemctl stop nginx
    systemctl stop coturn

    # création des backups des anciens certificats
    mkdir /opt/backup_certs
    cp /etc/pki/tls/certs/gofast.crt /opt/backup_certs/gofast.crt
    cp /etc/pki/tls/private/gofast.key /opt/backup_certs/gofast.key

    # Ensuite, on lance le script acme.sh (situé dans /opt), et on copie les certificats dans le bon répertoire.
    ./acme.sh --renew -d $gofast_comm --insecure
    cp /root/.acme.sh/$gofast_comm/$gofast_comm.cer  /etc/pki/tls/certs/gofast.crt
    cp /root/.acme.sh/$gofast_comm/$gofast_comm.key /etc/pki/tls/private/gofast.key
    
    # Enfin, on redémarre les services :
    systemctl start nginx
    systemctl start coturn

