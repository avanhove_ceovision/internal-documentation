# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020
# This file is distributed under the same license as the internal-documentation package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: internal-documentation latest\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-21 17:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: /home/jleman/SSD/dev/internal-documentation/README.md:1
msgid "Internal Documentation"
msgstr ""

#: /home/jleman/SSD/dev/internal-documentation/README.md:3
msgid "Internal documentation and procedures for the IT service"
msgstr ""
