*********************************************************
PROC -  Update osTicket vers 1.15.8
*********************************************************

Informations
============

===============  ==========  ============= ==================================
Instance           DC         Hyperviseur   Sujet de la MAJ
===============  ==========  ============= ==================================
PRD-CEOV-SRV01    OVH/SBG3    OVH/os-sbg6   Mise à jour de version osTicket
===============  ==========  ============= ==================================

Pré-requis
============

- Accès admin à la machine
- Un snapshot VM et vérifier les backups VEEAM dans le cas d'une erreur
- Un backup des fichiers web (``tar -cvf <SOURCE> <DESTINATION>`` - normalement /var/www/osticket) 
- Un backup de la base de données
- Serveur web apache (*pas de version spécifié*)
- PHP version 7.4
- MySQL version 5.5
- Les sources de la mise à jour (à télécharger sur `osticket.com <https://osticket.com/download/>`_)

Installation
=============

Backup des dossiers & fichiers
------------------------------
On commence par vérifier les versions de PHP et MySQL:
::
    php -v
    mysql -v --password
Puis, on backup le répertoire **/var/www/osTicket**:
::
    cd /var/www/
    tar -cvf osticket_backup.tar.gz osticket/
On récupère également le fichier de configuration **ost-config.php** situé dans **osticket/include**.
::
    cp ./osticket/include/ost-config.php /tmp

Mise à jour de PHP
------------------
PHP est en version **7.1.33**, il faut le passer en version **7.4**.

.. image:: img/version_php_srv01_prod_osticket.png

Voici les commandes à passer:
::
    # ajout des repos
    wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    wget https://rpms.remirepo.net/enterprise/remi-release-7.rpm --no-check-certificate
    rpm -Uvh remi-release-7.rpm epel-release-latest-7.noarch.rpm
    #disable php 7.1 et enable php 7.4
    yum-config-manager --enable remi-php74
    yum-config-manager --disable remi-php71
    # delete de php 7.1
    yum -y remove php71*
    # install de la version 7.4
    yum -y install php php-cli php-fpm php-mysqlnd php-zip php-devel php-gd php-mcrypt php-mbstring php-curl php-xml php-pear php-bcmath php-json php-opcache php-xdebug php-memcache php-ldap
    # restart des services
    systemctl enable php-fpm
    systemctl start php-fpm
    systemctl restart httpd
    
.. note::
    Si tout est ok, osTicket doit fonctionner comme avant.

Upload des fichiers de mise à jour
----------------------------------
Dans le fichier *.zip* qui contient les sources de la mise à jour, il faut extraire son contenu pour garder uniquement le dossier upload et les fichiers *.phar* (fichiers de langage).

.. image:: img/folder_update_srv01_prod_osticket.png

Via le moyen que vous préférez : SFTP ou SCP etc.. envoyez les fichiers du dossier **upload** dans le répertoire **/var/www/osticket**.

.. note::
    screenshot à faire lors de la MAJ en prod.
    
Puis, déposer les fichiers *.phar*, dans le répertoire **/var/www/osticket/include/i18n/**.

.. note::
    screenshot à faire lors de la MAJ en prod.

Sur le navigateur, actualisez la page, un bouton permettant de lancer l'upgrade doit s'afficher.

.. note::
    screenshot à faire lors de la MAJ en prod.

Cliquez sur celui-ci et l'upgrade démarre.
À la fin de la mise à jour, osTicket vous demandera de supprimer le dossier **setup**.
::
    cd /var/www/osticket/
    rm -rf setup/

La mise à jour est terminée.

Documentations & sources
------------------------
- https://docs.osticket.com/en/latest/Getting%20Started/Upgrade%20and%20Migration.html
- https://osticket.com/download/

Troubleshooting
==============
Cette partie recense tous les problèmes rencontrés lors de la mise à jour.

.. note::
    Aucun à ce jour :)

