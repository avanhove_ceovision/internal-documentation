CEO-Vision Documentation Interne
####################

.. _it-kickstart:

.. toctree::
   :maxdepth: 1
   :caption: Documentation IT Générale

   it-kickstart/access-tools
   it-kickstart/tempo

.. _dev-gofast:

.. toctree::
   :maxdepth: 1
   :caption: Documentation développeur

   dev-gofast/keen
   dev-gofast/jira
   dev-gofast/technical-translations
   dev-gofast/update-drupal
   dev-gofast/coding-convention
   dev-gofast/check-git-end-versions

.. _dev-ci_cd:

.. toctree::
   :maxdepth: 1
   :caption: Documentation CI/CD

   dev-ci_cd/git-doc
   dev-ci_cd/git-proc

.. _prod-gofast:

.. toctree::
   :maxdepth: 1
   :caption: Documentation Production GoFAST

   prod-gofast/install
   prod-gofast/update
   prod-gofast/cartificate
   prod-gofast/veeamRestore
   prod-gofast/esx-secure
   prod-gofast/createSpace

.. _prod-internal:

.. toctree::
   :maxdepth: 1
   :caption: Documentation Production Interne

   prod-internal/osticketUpgrade
   prod-internal/installNewOsTicket

