# Donner des droits d'accès à un espace

- userd_uid - 4 pour adm dans la majorité des cas

## Pour un utilisateur
```php
$space_id = 126839;
$user_uid = 61;
 // ( autres valeurs : "read only member" et "group contributor" )
$role_to_apply = "administrator member";
$og_role = gofast_og_space_admin_get_node_role('node', $space_id, $role_to_apply);
$og_role_id = key($og_role);

gofast_og_space_admin_add_membership($space_id, $user_uid, $og_role_id);
```

## Pour une liste d'utilisateur
```php
$space_nid = 5973;
$space_type = 'organisation' ; // group, extranet, public, organisation
$userlist_nid = 190011;
$ulid = node_load($userlist_nid)->field_userlist_ulid[LANGUAGE_NONE][0]['value'];

$g_roles_query = og_roles('node', $space_type , $space_nid, FALSE, FALSE);

foreach($g_roles_query as $rgkey => $role ){

      if($role == 'administrator member' ){ // cas "admin"

        $admin_rgid = $rgkey;

      }

}

gofast_userlist_og_add_membership($space_nid , $ulid, $admin_rgid);
```