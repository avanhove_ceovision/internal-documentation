## Commandes utiles
---

- Affichage des fichiers/répertoires  selon la taille
```bash
ls -lathS
```
- Afficher l'espace utilisé et disponible sur le système
```bash
df -h
```
- Afficher l'espace utilisé et disponible pour les fichiers/dossiers
```bash
du -h
```
- Afficher l'espace utilisé et disponible pour les fichiers/dossiers *(pronfondeur de 1)*
```bash
du -h --max-depth=1
```
- Vérifier les volumes :
```bash
ls -l /dev/[svx][vd]*
```
- Vérifier l'espace non partionné.
```bash
parted
(parted) print free
```
- Lister / modifier les partitions
```bash
fdisk -l
fidsk /dev/sda
```
- Lister les Volumes Groups
```bash
vgdisplay
```
- Lister les Volumes Logiques
```bash
lvdisplay
```

## Extension du disque
---
- Rescan du stockage (dans le cas ou le disque n'est pas ISCSI)
```bash
echo 1 > /sys/class/block/sda/device/rescan

```

- Rescan du stockage **(dans la majorité des cas)** *(avec tabulation)*
```bash
echo 1 > /sys/class/scsi_device/<TAB>/device/rescan
# exemple
echo 1 > /sys/class/scsi_device/0\:0\:0\:0/device/rescan
echo 1 > /sys/class/scsi_device/0\:0\:1\:0/device/rescan
echo 1 > /sys/class/scsi_device/3\:0\:0\:0/device/rescan
```

- Création d'une partition
```bash
fdisk /dev/sda
n # new partition
p # primary part
# enter
# enter
t
# partition number
8e
w # write
partprobe
```

- Ajout de la partition à LVM

Vérifier si c'est : **/dev/mapper/centos-var** ou **/dev/mapper/centosroot**

```bash
# Check if the partition is visible
fdisk -l
pvcreate /dev/sdaX # X -> replace with part number
vgextend centos /dev/sdaX
lvextend -l+100%FREE /dev/mapper/centos-var # all free spaces
#lvextend -L +100G /dev/.... (for extend only +100Gb) 
#lvextend -L100G /dev/..... (for extend lvm to 100G, not +100G)
xfs_growfs /dev/mapper/centos-var
df -h
```
