
## Source
---
https://community.ceo-vision.com/topic/96/acc%C3%A8s-session-avec-identifiant-ad/3?_=1660216334547

## Procédure
---
- Vérifier que saslauthd est correctement installé et supporte le mechanisme LDAP.
```bash
saslauthd -v
```

- Activer le mechanisme LDAP et ajouter le flag -O aux options de démarrage du demon.
```sh
vi /etc/sysconfig/saslauthd

#ajouter
SOCKETDIR=/var/run/saslauthd
MECH=ldap
FLAGS="-O /etc/saslauthd.conf"
```

- Communication entre OpenLDAP et saslauthd.
```sh
vi /usr/lib64/sasl2/slapd.conf # ou vi /usr/lib64/sasl2/slapd.conf

# ajouter
pwcheck_method: saslauthd
saslauthd_path: /var/run/saslauthd/mux
```

- Configuration de l'accès LDAP
```sh

vi /etc/saslauthd.conf
ldap_servers: ldaps://ldap-preprod.ceo-vision.com
ldap_search_base: ou=people,dc=ceo-vision,dc=com
ldap_timeout: 10
ldap_filter: (uid=%u)
ldap_bind_dn: cn=Manager,dc=ceo-vision,dc=com
ldap_bind_pw: XXXXXXXXXXXXXXXXXXXXXXXX
ldap_password: XXXXXXXXXXXXXXXXXXXXXXXX
ldap_deref: never
ldap_restart: yes
ldap_scope: sub
ldap_use_sasl: no
ldap_start_tls: no
ldap_version: 3
ldap_auth_method: bind
```

- Redémarrage et test 
```bash
systemctl enable saslauthd
systemctl restart saslauthd
systemcl restart slapd
testsaslauthd -u user -p password
```
