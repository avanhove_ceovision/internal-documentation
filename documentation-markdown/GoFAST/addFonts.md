# Procédure : Ajouter des polices dans GoFAST
---
## Pré-requis
Les polices d'écritures au format **.ttf**.

## Installation
---
Sur la machine **MAIN** & **COMM**, déposer les fichiers dans le répertoire **/usr/share/fonts**.
Puis, sur la machine COMM, il faut lancer le script suivant :
```bash
/usr/bin/documentserver-generate-allfonts.sh
```
*Remarque: pour que les polices soient disponible dans le navigateur, il est possible que l'utilisateur doive vider le cache du navigateur*

## Exemple
---
```bash
wget https://freefontsdownload.net/download/66334/arial_narrow.zip
yum -y install unzip
unzip arial_narrow.zip
cp ArialNarrow* /usr/share/fonts
/usr/bin/documentserver-generate-allfonts.sh
```
