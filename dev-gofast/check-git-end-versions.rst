****************************************************************************************
DOC - Jira : Gestion de Git en fin de hotfix ou release candidate intermediaire
****************************************************************************************

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
  AVA          20/04/2023       V1.1
========= ==================== =======

Workflow de développement
==============================================

Cette documentation décrit le processus de vérification GIT en fin de hotfix pour éviter tout problème lors du déploiement, et aussi décrire les bonnes pratiques pour la gestion des branches/tags

.. WARNING::
   Seul une personne autorisée peut se permettre de se lancer dans cette procédure de mise à jour.

Vérification du nombre de fichiers dans la version
===================================================

Git gofast3
------------

1. Vérifier le nombre de fichiers dans la version, il faut se rendre dans le répertoire de la version dans le git d'install (à jour sur la branche de la version souhaitée) et lancer la commande suivante :

:: 
    
    ll | wc -l

2. Checkout sur Master
3. Se baser sur la version précédente (Hard reset vers le tag) dans Git pour vérifier que le nombre de fichiers modifiés depuis corresponds au nombre de fichiers remontés par la commande précédente
4. Merge la branche de la version souhaitée dans la branche master
5. Comparer le nombre de fichiers modifiés depuis le tag de la version précédente avec le nombre de fichiers modifiés depuis le tag de la version souhaité
6. Le nombre de fichiers doit être identique (+ ou - si il y a des fichiers qui ne sont pas dans les sources GoFAST)
7. Remettre master à jour (revenir à l'état actuel de la branche master)
8. Merger la version souhaitée dans la branche master
9. Créer le tag sous la forme de "RELEASE 4.X.X HFX X.X : Merge branch dev/4.X.X_hotfix_x.x into master"
10. Pousser le tag sur origin

Git gofast-install
-------------------

1. Comparer le nombre de fichiers dans la version avec le nombre de wget dans le fichier update_gofast.sh
2. Comparer le nombre de fichiers dans le repertoire d'update de la comm avec le nombre de wget dans le fichier gofast-comm-update.sh
3. Merger la version souhaitée dans la branche Master
4. Créer le tag sous la forme de "RELEASE 4.X.X HFX X.X : Merge branch dev/4.X.X_hotfix_x.x into master"
5. Pousser le tag sur origin


Lancement de la mise à jour sur la plateforme de preproduction
================================================================

1. Lancer la mise à jour sur la plateforme de preproduction
2. Vérification de potentielles erreurs dans le fichier de log (update_report.gflog) de la MAIN
3. Pareillement sur le serveur de la COMM


