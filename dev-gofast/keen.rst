********************************************
DOC - Theme GoFAST 4 : Keen
********************************************

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
  AVA          01/09/2021      V1.1
========= ==================== =======

Installation du thème Keen
==============================================

Téléchargez le thème Keen depuis la page `WordPress Themes & Website Templates from ThemeForest <http://themeforest.com>`__

Dans le dossier, il y a 3 sous-dossiers, le premier est l'endroit où viennent les fichiers de conception (dans Sketch - éditeur graphique pour macOS), le second est la documentation et le troisième est le code source du theme. A l'intérieur du dossier thème on trouve plusieurs dossiers avec le nom 'demo' et un numéro, cela fait référence aux différents styles disponibles par Keen, on trouve aussi le dossier 'tools' qui est la base des autres styles. Nous pour travailler on a choisi le Demo1

Faire 'Build' du theme en suivant la documentation keen, on a fait la implementation avec webpack en nodejs `Premium Bootstrap Admin Dashboard Themes <https://keenthemes.com/keen/?page=docs&section=html-webpack-build-tools>`__

Une fois que nous avons notre dossier 'bundle', nous le mettons à la racine de notre thème Drupal appelé 'bootstrap-keen'

Importer les fichiers scripts.bundle.js  prismjs.bundle.js plugins.bundle.js plugins.bundle.css prismjs.bundle.css  style.bundle.css dans le fichier “bootstrap-keen.info“

Pour mettre à jour les bibliothèques du thème Keen, vous devez suivre la documentation keen  et faire un « rebuild » du theme `Premium Bootstrap Admin Dashboard Themes <https://keenthemes.com/keen/?page=docs&section=update>`__


Mise en page principale
==============================================

Dans le fichier page.tpl.php on a mit le coeur de notre layout, il est responsable du rendu de toutes les pages principales de l'application, cela signifie que c'est le modèle qui est toujours utilisé dans toute l'application. Le modèle est l'endroit où nous avons construit le système de navigation de notre application. Pour rendre le contenu des pages Drupal utilise la variable $ page ['content'] au sein du template page.tpl.php, qui est en fait une région où les pages sont rendues sous forme de blocs, pour le moment on utilise plus le sisteme des régions Drupal.

On créé une solution avec des functions et des hooks Drupal pour faciliter le rendu de notre contenu car actuellement dans l'application il y a des pages avec des mises en page différentes, on peut généralement trouver 3 styles de page, ce sont :

- Page entière
- Page avec bloc à droite
- Page avec bloc à gauche

Structure de la page page.tpl.php:
-----------------------------------

.. image:: img/main-layout.png

Creation du contenu de la page:
-----------------------------------

Pour résoudre et faciliter le rendu de toutes nos pages, nous avons créé une fonction appelée gofast_create_page_content() dans le ficher gofast.module du module gofast, cette fonction est en charge de créer le framework global où nous allons visualiser le contenu. Pour cela, nous avons créé 5 templates auxiliaires dans le meme module gofast qui nous permettent de vous donner de la flexibilité et ainsi de prendre en charge toutes les pages qui ont une mise en page spécifique.

.. image:: img/main-layout-files.jpg

Cette fonction reçoit doit recevoir en paramètre le contenu à restituer et peut également recevoir en paramètres optionnels, le type de page et le bloc de contenu latéral pour les pages qui doivent afficher un bloc à gauche ou à droite.

.. image:: img/create-page-content.jpg

Exemple
-----------------------------------

Pour rendre une page spécifique lorsqu'un utilisateur clique sur n'importe quelle URL, le moteur Drupal recherchera tout le hook_menu de l'application et exécutera la fonction associée qui devrait renvoyer le contenu à rendre par Drupal. Ce que nous avons décidé de faire, c'est qu'avant de rendre le contenu directement, nous allons passer par la fonction gofast_create_page_content () pour créer le cadre idéal où vivra notre contenu, de cette manière il est plus facile de gérer et de prendre en charge la mise en page globale du application.

.. image:: img/exemple.jpg


Pour gérer l'affichage de notre conteneur principal, nous avons utilisé le système Grid dans notre ficher ‘gofast-content.tpl.php' que c’est le container principale ou on va visualiser nos pages, cela nous permet de sélectionner notre page et de localiser les blocs internes avec une plus grande flexibilité.

.. image:: img/maint-content.png

Avec ce système de grille et à l'aide du media queries en scss (@media), nous pouvons gérer les dimensions des blocs latéraux pour chaque résolution, en plus quand nous pourrions également gérer lorsqu'il n'y a pas de blocs supplémentaires, ce qui permet de rendre toutes les pages avec ce composant simple.

.. image:: img/gofast-content.jpg

.. image:: img/gofast-content2.jpg


Gestion des styles
==============================================

Pour la gestion des styles nous utilisons le processeur 'SASS' que nous compilons ensuite à l'aide de webpack et node js pour faciliter la compilation et l'écriture de feuilles de style CSS.

Installation
-----------------------------------

- Add NodeSource yum repository

.. code-block::

  curl -sL https://rpm.nodesource.com/setup_16.x | sudo bash -

- Install Node.js and npm

.. code-block::
  sudo yum install nodejs

- Create gofast4-bundle in bootstrap-keen theme

- Inside gofast4-bundle create a package.json file (Generate it without having it ask any questions)

.. code-block::

  npm init -y

- Instal webpack and others dependencies

.. code-block::

  npm install [name-dependence]

"css-loader": "^6.2.0"

"file-loader": "^6.2.0"

"sass": "^1.38.0"

"sass-loader": "^12.1.0"

"style-loader": "^3.2.1"

"url-loader": "^4.1.1"

"webpack": "^5.50.0"

"webpack-cli": "^4.8.0"

- Create folder src inside gofast4-bundle, copy and paste scss folder into src

- Create configuration webpack - webpack.confing.js in the same level of package.json

- Add build command in package.json

.. code-block::

  "scripts": {"build": "webpack --config webpack.config.js"},

- Build

.. code-block::

  npm run build

.. warning::  Il faut importer seulement le fichier style.min.css qui est dans le dossier ‘dist'….. Il faut faire attention de pas déployé le dossier node_modules
  (A l'intérieur du sous-dossier ‘src', nous trouverons un fichier appelé 'style.scss' à la racine, c'est dans ce fichier que nous allons importer toutes les feuilles de style de nos composants. Webpack est configuré pour lire ce fichier et créer la compilation en 'CSS' dans un nouveau dossier à la racine de 'bundle-gofast4' appelé 'dist' dans lequel on retrouve le fichier 'style.css.min'  qu’il faut importer dans le fichier 'bootstrap-keen.info')


Structure 'bundle-gofast4'
-----------------------------------

C'est dans ce dossier que nous allons gérer les feuilles de style scss et les fichiers package.json et la configuration du pack web. A l'intérieur du sous-dossier 'src', nous trouverons un fichier appelé 'style.scss' à la racine, c'est dans ce fichier que nous allons importer toutes les feuilles de style de nos composants. Webpack est configuré pour lire ce fichier et créer la compilation en 'CSS' dans un nouveau dossier à la racine de 'bundle-gofast4' appelé 'dist' dans lequel on retrouve le fichier 'style.css.min' dans lequel il faut importer le fichier 'bootstrap-keen.info'.

.. image:: img/style_tree.png

.. image:: img/tree-gofast4bundle.png

Gestion des variables (Variables.scss)
-----------------------------------

.. important:: `Documentation SASS <https://sass-lang.com/documentation>`_


Ce fichier est l'endroit où nous allons gérer toutes les variables de style telles que la typographie, les points d'arrêt pour les tailles d'écran, les couleurs, etc.. Pour plus d'informations sur l'utilisation et la création de variables lisez la documentation ci-dessous

.. image:: img/scss-img.png

.. image:: img/scss-img2.png


**Resolutions d'ecrans pris en charge**

Nous définissons 5 types de résolutions que l'application prendra en charge (exclusivement laptop), ce sont les résolutions en pixels :

- petits appareils : 1280 x 768 (résolution minimale)
- appareils moyens : 1366 x 800
- gros appareils : 1440 x 900
- appareils xlarge : 1600 x 1080
- xxlarge-devices :> 1920 x 1080
