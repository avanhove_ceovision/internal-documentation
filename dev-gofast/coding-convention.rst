****************************************************************************************
DOC - Jira : Conventions de codage de l'entreprise
****************************************************************************************

========= ==================== =======
Rédacteur Date de modification Version
========= ==================== =======
  AVA          09/10/2023       V1.0


Introduction
------------

Ce document présente les conventions de codage à suivre au sein de notre entreprise pour les langages de programmation JavaScript et PHP, ainsi que les bonnes pratiques spécifiques pour le développement de modules Drupal 7. Ces conventions visent à assurer une cohérence dans le code source, à améliorer la lisibilité du code et à faciliter la collaboration entre les développeurs.

Convention générale
-------------------

Décomposition des fonctions
~~~~~~~~~~~~~~~~~~~~~~~~~~

Il est essentiel de décomposer les grosses fonctions en sous-fonctions avec des noms clairs et des responsabilités bien définies. Cette approche, appelée "décomposition fonctionnelle", rend le code plus modulaire, plus facile à lire et à maintenir. Chaque sous-fonction devrait accomplir une tâche spécifique et avoir un nom qui reflète clairement cette tâche.

La décomposition fonctionnelle permet de diviser des problèmes complexes en étapes plus gérables, facilitant ainsi le développement et la collaboration entre les développeurs. Elle encourage également la réutilisation du code, car les sous-fonctions peuvent être utilisées dans d'autres parties du programme si nécessaire.

Nommage des fichiers
~~~~~~~~~~~~~~~~~~~~

Pour assurer une cohérence dans les noms de fichiers, nous devons suivre les conventions de casse spécifiques pour chaque langage de programmation.

- En PHP pour les fichiers Drupal (par exemple, `.module`, `.info`, etc.), nous utilisons la convention **Snake Case** pour les noms de fichiers. Le Snake Case consiste à écrire les mots composant le nom en les séparant par des underscores.

- En JavaScript, nous utilisons la convention **Camel Case** pour les noms de fichiers. Le Camel Case consiste à écrire les mots composant le nom sans espace, en mettant une majuscule au début de chaque mot, à l'exception du premier mot.

Conventions de codage pour JavaScript
-------------------------------------

Camel Case
~~~~~~~~~~

La convention de codage pour les noms de variables et de fonctions en JavaScript est le Camel Case. Le Camel Case consiste à écrire les mots composant le nom sans espace, en mettant une majuscule au début de chaque mot, à l'exception du premier mot.

Exemple en JavaScript::

    // Calculate and return the average of an array of numbers.
    function calculateAverage(numbersArray) {
        // Make sure the array is not empty to avoid division by zero.
        if (numbersArray.length === 0) {
            return 0;
        }

        let sum = 0;
        for (const number of numbersArray) {
            sum += number;
        }

        // The average is the sum of numbers divided by their total count.
        const average = sum / numbersArray.length;
        return average;
    }

    // Example of variables using Camel Case
    const userName = "JohnDoe";
    let userAge = 30;
    const isUserActive = true;

Bonnes pratiques en JavaScript
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Utiliser les déclarations de variables avec `let` ou `const`: Évitez d'utiliser `var` pour déclarer des variables, préférez `let` pour les variables dont la valeur peut être modifiée et `const` pour les variables dont la valeur est constante.

2. Utiliser les fonctions fléchées (Arrow functions) : Les fonctions fléchées offrent une syntaxe plus concise pour les fonctions et lient automatiquement la valeur de `this` au contexte dans lequel elles sont définies.

3. Utilisation de notre librairie JQuery et pas d'autres librairies : Nous utilisons notre librairie JQuery pour manipuler le DOM et effectuer des requêtes AJAX. Nous n'utilisons pas d'autres librairies telles que Angular, React ou Vue.js ou meme le javascript natif.

4. Utiliser des opérateurs ternaires : Les opérateurs ternaires (`condition ? valeurSiVrai : valeurSiFaux`) sont utiles pour écrire des expressions conditionnelles courtes et concises.

5. Gérer les erreurs avec des blocs try-catch : Utilisez des blocs `try-catch` pour capturer et gérer les erreurs de manière appropriée, ce qui permet de mieux gérer les situations d'erreur dans votre application.

6. Utiliser des modules pour l'organisation du code : Organisez votre code en modules pour éviter les conflits de noms et améliorer la maintenabilité.

Conventions de codage pour PHP
------------------------------

Snake Case
~~~~~~~~~~

La convention de codage pour les noms de variables et de fonctions en PHP est le Snake Case. Le Snake Case consiste à écrire les mots composant le nom en les séparant par des underscores.

Exemple en PHP::

    // Function to display the result in the console.
    function display_result($result) {
        echo "The result is: " . $result;
    }

    // Function call.
    $sum = calculate_sum(5, 10);
    display_result($sum);

Scénarios de tests
------------------

Avant la mise en "Test Phase", chaque développeur est responsable d'écrire les scénarios de tests associés à leur fonctionnalité ou correction. Ces scénarios doivent être inclus dans la tâche Jira qui leur a été attribuée.

Après avoir développé le code et rédigé les scénarios de tests, le développeur doit suivre les étapes suivantes :

1. Ajouter les scénarios de tests dans la tâche Jira qui lui a été attribuée.
2. Soumettre le code pour la revue du reviewer.
3. Après la validation par le reviewer, marquer la tâche comme "Test Phase" dans Jira.
4. Intégrer les scénarios de tests dans TestRail pour une gestion centralisée des tests.

Drupal 7
--------

Lors du développement de modules pour Drupal 7, suivez ces bonnes pratiques pour assurer un code propre et maintenable :

1. Utiliser les hooks : Placez les hooks dans le fichier `.module` pour interagir avec le système Drupal et réagir à des événements spécifiques.

2. Organiser le code en sections logiques : Divisez le code de votre module en sections logiques pour faciliter la lisibilité et la maintenance.

3. Utiliser les fichiers séparés pour les fonctionnalités complexes : Si votre module implémente des fonctionnalités complexes, envisagez de les séparer dans des fichiers spécifiques pour mieux organiser le code.

4. Créer des sous-dossiers pour les éléments connexes : Organisez les ressources supplémentaires, telles que des fichiers CSS, des images ou des modèles, dans des sous-dossiers dédiés pour une meilleure séparation des préoccupations.

Conclusion
----------

En adoptant ces conventions de codage et bonnes pratiques, notre équipe de développement peut bénéficier de nombreux avantages. Nous assurons une cohérence dans le code source, améliorons la lisibilité du code et facilitons la collaboration entre les membres de l'équipe. De plus, ces pratiques nous aident à maintenir un code de qualité, réutilisable et compatible avec le système Drupal 7.

En mettant en pratique ces principes dans notre travail quotidien, nous renforçons la qualité, la robustesse et la maintenabilité de notre code, contribuant ainsi à la réussite de nos projets de développement. L'adoption de ces conventions et bonnes pratiques fait partie intégrante de notre engagement envers l'excellence technique et l'amélioration continue de nos produits logiciels.

.. raw:: html

    <hr>

.. raw:: html

    .. footer:: 2023, CEO-Vision
