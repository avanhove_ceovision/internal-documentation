********************************************
DOC - Installation GoFAST
********************************************

Informations nécessaires
==============================================

- URL (pour la MAIN, MOBILE et la COMM)
- Adresses IP pour les deux machines (si ESXi)
- Les informations sur les certificats (wildcard ou let's encrypt)
- Serveur SMTP (mail)

Partitionnement Machines
==============================================


====  ========
FREE  /var
4Go   /tmp
2Go   /var/log/audit
10Go  /var/log
1Go   /var/tmp
8Go   SWAP
10Go  /root
====  ========

FREE = espace libre variable en fonction de la place restante sur le disque (après partitionnement de toutes les autres partitions)

Assignation IP/MAC
==============================================

https://gofast-docs.readthedocs.io/fr/latest/docs-gofast-technical/gofast-docs-prerequis-installation-serveur.html#configuration-du-reseau-par-lexploitant

assignation ip sur esx :
https://ceovision.atlassian.net/browse/CEOVIS-398

Besoin :

- @IP
- @MAC (associé à l'IP)
- Gateway (ESX)

.. image:: img/gateway_esx.png

Nous allons commencé par l'assignation de l'adresse MAC à la machine virtuelle voulue. (machine éteinte)

Rendez-vous sur la machine et cliquer sur l'onglet "modifier" puis scrollez jusqu'à voir "adaptateur réseau 1".

Déroulé ensuite le menu et passé l'adresse MAC en manuel et venait y coller votre nouvelle @MAC (celle assignée à votre IP)

.. image:: img/address_mac.png

Vous pouvez donc ensuite démarrer votre machine virtuelle, nous allons lui mettre son adresse IP.

Entrez la commande "nmtui" qui vous ouvrira une interface pour configurer l'adresse IP de votre machine.

::

  nmtui


.. image:: img/edit_connection.png

.. image:: img/edit_connection2.png

Vous pouvez désormais changer votre adresse IP, gateway ainsi que votre serveur DNS (8.8.8.8).

Cependant, votre machine ne sera pour le moment pas apte à communiquer avec l'extérieur.

Effectuez donc cette commande :

::

  vi /etc/sysconfig/network-scripts/ifcfg-ens192


Et passez la partie "ONBOOT" de "no" à "yes"

.. image:: img/onboot.png

( ":wq" pour quitter vi en sauvegardant)

puis pour finir :

::

  systemctl restart network

Désormais la machine est censée pouvoir ping l'extérieur.

::

  ping google.fr


Installation GoFAST
==============================================

Variables pour l'exécution du script d'install :

Script MAIN :

- $1 = version de GoFAST (que l'on souhaite installer, attention à être sur la bonne branche du git d'install pour avoir le bon script voulu)
- $2 = "enterprise" ou "community"
- $3 = enterprise key main (si installation "enterprise) http://dev2.ceo-vision.com/monitoring/manage_customers.php

Script COMM :

- $1 = version de GoFAST
- $2 = enterprise key comm

Aller déposer en FTP dans le /opt de chaque machine les fichiers/dossiers nécessaires à l'installation de GoFAST.

MAIN : 

.. image:: img/files_main.png

à déposer dans /opt de la machine virtuelle

::

  cd /opt
  yum install -y dos2unix
  chmod +x *.sh 
  dos2unix *.sh


COMM :

.. image:: img/files_comm.png

à déposer dans /opt de la machine virtuelle

::

  cd /opt/gofast-comm
  yum install -y dos2unix
  chmod +x *.sh 
  dos2unix *.sh


Choix du mot de passe
==============================================

.. warning::
  ATTENTION, prendre un mot de passe contenant :

- minimum 1 chiffre
- minimum 1 lettre minuscule
- minimum 1 lettre majuscule
- minimum 1 caractère spécial (ne pas prendre : % ' $ \ " )

Il nous faudra trois mots de passes :

- mot de passe "root, admin, bdd..." MAIN
- mot de passe "root, jitsi" COMM 
- mot de passe "adm"

(bien les renseigner dans TeamPass avant de les perdres)

Execution script d'installation/configuration
===================================================

Il ne vous reste plus qu'à lancer le script d'installation pour la machine.

MAIN :

::

  cd /opt
  ./gofast-3.x-install.sh 3.10.0 enterprise enterprise_key_main

.. warning::
  ATTENTION : n'oubliez pas de changer les trois variables (version, "enterprise" ou "community" et la clef enterprise)

  
COMM :

::

  cd /opt/gofast-comm
  ./gofast-comm-install.sh 3.10.0 enterprise_key_comm

.. warning::
  ATTENTION : n'oubliez pas de changer les variables de version et clef enterprise comm.

Une fois le script d'install fini pour la COMM, lancé le script de configuration.

::

  cd /opt/gofast-comm
  chmod +x *.sh 
  dos2unix *.sh 
  ./gofast-comm-config.sh

Lorsque ce script vous demande les certificats, si vous les avez, collez les, sinon prennez ceux d'une plateforme comme notre Cloud (pas permanent).


Correctifs à effectuer
===================================================

::

  vi /var/www/d7/sites/default/settings.php

Rendez-vous tout en bas de ce fichier ( ":9999999" pour aller tout en bas avec vi)

Et changez cela avec le prefix "mobile" ainsi qu'en rajoutant l'URL de votre COMM.

.. image:: img/domain.png

rajoutez également ceci tout en bas de votre fichier (en changeant les variables bien sur) :

::

    $conf['enterprise-key'] = "$enterprise_key_main";
    $conf['gofast-server-key'] = 'PRD-$quadigrame-GF-MAIN';

Exemple : 

::

    $conf['enterprise-key'] = "gofast-enterprise-e8c9cb3f-8ccf-4d02-a823-51c47d292f84";
    $conf['gofast-server-key'] = 'DEV-CEOV-GF-MAIN';


Déploiement Certificats
===================================================

Wildcards :
-------------

MAIN :

déposez le dossier contenant les certificats dans /opt

- copiez le certificat vers /etc/pki/tls/certs/localhost.crt 
- copiez la clef vers /etc/pki/tls/private/localhost.key 
- copiez le server-chain vers /etc/pki/tls/certs/server-chain.crt 

Vérifier les mappings des certificats dans les fichiers de configuration NGINX et APACHE :

/etc/nginx/nginx.conf 
/etc/httpd/conf.d/ssl.conf


  
COMM :

- copiez le certificat vers /etc/pki/tls/certs/gofast.crt 
- copiez la clef vers /etc/pki/tls/private/gofast.key 
- 
 .. code-block::

  cat /etc/pki/tls/certs/gofast.crt /etc/pki/tls/private/gofast.key > /etc/pki/tls/certs/gofast.pem

Tester les services (coo-édition, cadenas coo-édition, visio, chat Element), corriger si ceux-ci ne fonctionnent plus.


Let's Encrypt :
-----------------



Installation du paquet pour la génération des certificats (à faire sur les deux machines) :

::

  yum install -y certbot


MAIN :

Stop du service HTTPD + ouverture port 80 firewalld

::

  systemctl stop httpd
  systemctl stop nginx

  firewall-cmd --permanent --add-port=80/tcp # ouverture
  firewall-cmd --reload 

Test génération certificats (changer l'URL de la plateforme) :

::

  read url_main
  read url_mobile

  certbot certonly --non-interactive --email support@ceo-vision.fr \
  --preferred-challenges http --standalone --agree-tos --renew-by-default \
  --webroot-path  /var/www/html -d $url_main --dry-run

    certbot certonly --non-interactive --email support@ceo-vision.fr \
  --preferred-challenges http --standalone --agree-tos --renew-by-default \
  --webroot-path  /var/www/html -d $url_mobile --dry-run

Si ce test réussi, vous pouvez alors éxécuter ce code :

::

  read url_main
  read url_mobile

  certbot certonly --non-interactive --email support@ceo-vision.fr \
  --preferred-challenges http --standalone --agree-tos --renew-by-default \
  --webroot-path  /var/www/html -d $url_main

  certbot certonly --non-interactive --email support@ceo-vision.fr \
  --preferred-challenges http --standalone --agree-tos --renew-by-default \
  --webroot-path  /var/www/html -d $url_mobile --dry-run


Copie des certificats aux bons emplacements :

::

  read url_main
  read url_mobile

  cp /etc/letsencrypt/live/$url_main/privkey.pem /etc/pki/tls/private/localhost.key
  cp /etc/letsencrypt/live/$url_main/cert.pem /etc/pki/tls/certs/localhost.crt 
  cp /etc/letsencrypt/live/$url_main/fullchain.pem /etc/pki/tls/certs/server-chain.crt 

  cp /etc/letsencrypt/live/$url_mobile/privkey.pem /etc/pki/tls/private/localhost_mobile.key
  cp /etc/letsencrypt/live/$url_mobile/cert.pem /etc/pki/tls/certs/localhost_mobile.crt

  firewall-cmd --permanent --remove-port=80/tcp → ouverture
  firewall-cmd --reload → reload

  systemctl start nginx
  systemctl start httpd

COMM :

Stop service coturn :

::

  systemctl stop coturn


Test génération certificats (changer l'URL de la plateforme) :

::

  read url_comm
  mkdir /var/www/html

  certbot certonly --non-interactive --email support@ceo-vision.fr \
  --preferred-challenges http --standalone --agree-tos --renew-by-default \
  --webroot-path  /var/www/html -d $url_comm --dry-run


Si ce test réussi, vous pouvez alors éxécuter ce code :

::

  read url_comm
  mkdir /var/www/html

  certbot certonly --non-interactive --email support@ceo-vision.fr \
  --preferred-challenges http --standalone --agree-tos --renew-by-default \
  --webroot-path  /var/www/html -d $url_comm


Copie des certificats aux bons emplacements :

::

  read url_comm

  cp /etc/letsencrypt/live/$url_comm/privkey.pem /etc/pki/tls/private/gofast.key
	cp /etc/letsencrypt/live/$url_comm/cert.pem /etc/pki/tls/certs/gofast.crt

	cat /etc/pki/tls/certs/gofast.crt /etc/pki/tls/private/gofast.key > /etc/pki/tls/certs/gofast.pem

  systemctl restart nginx
  sleep 2
  systemctl stat coturn

Tester les services (coo-édition, cadenas coo-édition, visio, chat), corriger si ceux-ci ne fonctionnent plus.

Potentiels problèmes et comment les résoudres
===================================================

ERREUR 500 BONITA (MAIN) :
----------------------------

::

    # Deploy userlists menu
    drush @d7 php-eval "include_once '/var/www/d7/sites/all/modules/gofast/gofast_userlist/gofast_userlist.install';usurp(1);gofast_userlist_create_menus();"

    #Change length of locations fields
    drush @d7 php-eval 'include_once "/var/www/d7/sites/all/modules/gofast/gofast_cmis/gofast_cmis.install"; gofast_cmis_update_7706();'


    #Deploy REST API Extensions
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('archived_variables.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('get_cases_from_nid.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('tasks_assigned_other.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('get_tasks_all_infos.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('get_tasks_from_caseids.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('process_search.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('tasks_in_process.zip')"

    #Deploy pages
    drush @d7 php-eval "gofast_workflows_deploy_page('page-LightDashboard.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_page('page-LightDashboardDocument.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_page('page-HistoryBdm.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_page('page-BlockDashboard.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_page('page-FullDashboardSearch.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_page('page-TasksInProcess.zip')"

    #Create and configure GoFAST application
    drush @d7 php-eval "gofast_workflows_deploy_gofast_application()"



Mauvais déploiement Workflow (MAIN) :
---------------------------------------

1 : se déconnecter de la plateforme

2 : supprimer le cookie dans /var/www/d7/sites/default/files/swf

::

    cd /var/www/d7/sites/default/files/swf
    rm -Rf cookie_bonitaadm.txt

3 : se reconnecter en navigation privée

4 : passer ces commandes :

:: 

    cd /opt
    \cp -Rf /var/www/d7/sites/all/modules/gofast/gofast_workflows/ressources/BDM/build/bdm.zip /var/cache/tomcat/temp/bonita_portal_*/tenants/1/
    chmod -R 777 /var/cache/tomcat/temp/
    drush @d7 php-eval "gofast_workflows_deploy_business_data_model();"

    #Deploy new standard workflow and map actors

    drush @d7 php-eval "gofast_workflows_deploy_new_version_standard_process('Diffusion de document--1.13.bar', 'Document broadcast--3.4.bar');"
    drush @d7 php-eval "gofast_workflows_deploy_new_version_standard_process('GoFast accuse reception de document--1.13.bar', 'Document broadcast subprocess--3.4.bar');"

    #Deploy REST API Extensions
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('archived_variables.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('get_cases_from_nid.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('tasks_assigned_other.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('get_tasks_all_infos.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('get_tasks_from_caseids.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('process_search.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_rest_api_extension('tasks_in_process.zip')"

    #Deploy pages
    drush @d7 php-eval "gofast_workflows_deploy_page('page-LightDashboard.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_page('page-LightDashboardDocument.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_page('page-HistoryBdm.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_page('page-BlockDashboard.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_page('page-FullDashboardSearch.zip')"
    drush @d7 php-eval "gofast_workflows_deploy_page('page-TasksInProcess.zip')"

    #Create and configure GoFAST application
    drush @d7 php-eval "gofast_workflows_deploy_gofast_application()"

    systemctl restart tomcat@bonita

.. warning::
  ATTENTION CHANGER LE CHEMIN DE DESTINATION POUR LES TROIS PROCHAINES COMMANDES "/var/cache/tomcat/temp/bonita_portal_29372\@gofast.ceo-vision.com//tenants/1/"
::

    \cp -Rf "/var/www/d7/sites/all/modules/gofast/gofast_workflows/ressources/workflows/document_broadcast/build/Document broadcast_3.4.png" "/var/www/d7/sites/default/files/"
    \cp -Rf "/var/www/d7/sites/all/modules/gofast/gofast_workflows/ressources/workflows/document_broadcast/build/Document broadcast--3.4.bar" /var/cache/tomcat/temp/bonita_portal_29372\@gofast.ceo-vision.com//tenants/1/
    \cp -Rf "/var/www/d7/sites/all/modules/gofast/gofast_workflows/ressources/workflows/document_broadcast/build/Document broadcast\ subprocess--3.4.bar" /var/cache/tomcat/temp/bonita_portal_29372\@gofast.ceo-vision.com//tenants/1/
    chmod -R 777 /var/cache/tomcat/temp/
    systemctl restart tomcat@bonita





Bcrypt (login une fois avec "adm" et après mot de passe faux) (MAIN) :
--------------------------------------------------------------------------

je créer le fichier enable-bcrypt.ldif

::

    cd /opt
    vi enable-bcrypt.ldif

coller ce code :

::

    # Add bcrypt support

    dn: cn=module{0},cn=config
    add: olcmoduleload
    olcmoduleload: /usr/lib64/pw-bcrypt.so

Executez ces commandes (en changeant la variable) :

::

    ldapmodify -w $mdp_root_main -x -D cn=config -f enable-bcrypt.ldif
    sealert -a /var/log/audit/audit.log
    systemctl restart slapd

changer le mot de passe du compte "adm" sur GoFAST

Problème Synapse ne démarre pas (COMM):
------------------------------------------

::

  cd /opt/synapse
  source bin/activate
  read server_name
  python -m synapse.app.homeserver --server-name $server_name --config-path homeserver.yaml --generate-config --report-stats=yes
  sudo curl https://raw.githubusercontent.com/ma1uta/matrix-synapse-rest-password-provider/master/rest_auth_provider.py -o /opt/synapse/lib/python3.6/site-packages/rest_auth_provider.py
  synctl start 
  deactivate


Problème BadGateway Nginx for OnlyOffice (COMM):
--------------------------------------------------

::

  erreur bad gateaway nginx for onlyoffice :

  echo "
    For Postgresql:
      Host: localhost
      Database: onlyoffice
      User: onlyoffice
      Password: onlyoffice
    For RabbitMQ:
      Host: localhost
      User: guest
      Password: guest"

  cd /opt/gofast-comm
  bash documentserver-configure.sh


Problème connexion non automatique au chat Element (COMM):
------------------------------------------------------------

Commandes à exécuter sur la plateforme COMM (en ssh) pour corriger le problème de connexion non automatique au chat.

::

  cd /opt/gofast-comm
  echo "Enter GoFAST MAIN URL (ex: gofast.ceo-vision.com):"
  read url_main
  java InstallCert $url_main
  \cp -Rf jssecacerts /etc/pki/ca-trust/extracted/java/cacerts
  systemctl restart ma1sd

Problème Cadenas OnlyOffice (MAIN):
-------------------------------------

Commandes à exécuter sur la plateforme MAIN (en ssh) pour corriger le problème de cadenas qui reste fermé à la sortie d'une coédition OnlyOffice.

::

  cd /opt
  echo "Enter GoFAST COMM URL (ex: gofast-comm.ceo-vision.com):"
  read url_comm
  java InstallCert internal-$url_comm
  \cp -Rf jssecacerts /etc/pki/ca-trust/extracted/java/cacerts
  echo "Warning la GED va redémarrer"
  sleep 4
  systemctl restart tomcat@alfresco

Problème Certificats Visio (COMM):
-------------------------------------

Commandes à exécuter sur la plateforme COMM (en ssh) pour corriger le problème de visio-conférence à trois minimum impossible.

::

  server_name=`grep -oP "(?<=domain: \').+?(?=\')" /etc/ma1sd/ma1sd.yaml`

  echo "secret_key = mot de passe dans /etc/jitsi/jicofo/config "

  read -s secret_key

  rm -Rf /var/lib/prosody/*
  cd /opt/gofast-comm/update

  systemctl stop crond
  prosodyctl stop
  systemctl stop jibri
  systemctl stop jitsi-videobridge
  systemctl stop jicofo

  #Generate certificates

  prosodyctl cert generate auth.${server_name}

  echo "
  #################################################
  ## Entrez les informations suivantes
  4096
  FR
  COMM
  GOFAST
  XMPP
  auth.${server_name}

  xmpp@auth.${server_name}
  #################################################"


  mv /var/lib/prosody/auth.$server_name.crt  /var/lib/prosody/auth.crt
  mv /var/lib/prosody/auth.$server_name.key  /var/lib/prosody/auth.key
  mv /var/lib/prosody/auth.$server_name.cnf  /var/lib/prosody/auth.cnf

  ln -sf /var/lib/prosody/auth.crt /etc/pki/ca-trust/source/anchors/auth.crt

  update-ca-trust extract -f

  prosodyctl start

  prosodyctl register videobridge auth.$server_name $secret_key
  prosodyctl register jibri auth.$server_name $secret_key
  prosodyctl register recorder recorder.$server_name $secret_key
  prosodyctl register focus auth.$server_name $secret_key
  prosodyctl mod_roster_command subscribe focus.$server_name focus@auth.$server_name

  systemctl start jibri
  systemctl start jitsi-videobridge
  sleep 30
  systemctl start jicofo
  systemctl start crond

og_user_node1 (MAIN):
------------------------

Se connecter en "admin" sur GoFAST, se rendre à cette URL https://$url/admin/config/people/accounts/fields
puis delete og_user_node1

Erreur Java - undefined symbol: JLI_Launch (COMM):
--------------------------------------------------

L'erreur complète est la suivante : **java: symbol lookup error: java: undefined symbol: JLI_Launch**
Cette erreur s'est produite après une restauration de la plateforme de DEMO pour la DEV (versionde GoFAST 4.1.0).

Pas d'explication sur son apparition ...
Pour résoudre ce problème, voici ce que j'ai fait : 

::
  
  yum install -y java-1.8.0-openjdk
  update-alternatives --config java # choisir la version 1.8 de Java
  java -version 
  yum remove java-11*
  yum remove -y java-1.8.0-openjdk
  yum remove -y java-1.8.0-openjdk-headless
  yum install -y java-11-openjdk java-11-openjdk-headless 
  update-alternatives --config java # chosir la version 11 de Java
  java -version

Erreur LDAP - Redémarrage du service à chaque connexion utilisateur:
--------------------------------------------------------------------

Pas d'erreur dans les logs LDAP et Drupal.
Il faut vérifier si le fichier ppolicy.ldif n'est pas dupliqué.

::
  
  locate 'olcOverlay={0}ppolicy.ldif' # et aller dans le répertoire 
  # ou 
  cd /etc/openldap/slapd.d/cn=config/olcDatabase={2}hdb/


Dans ce répertoire il ne doit pas y avoir olcOverlay={1}ppolicy.ldif.
Seul  olcOverlay={0}ppolicy.ldif doit être présent
